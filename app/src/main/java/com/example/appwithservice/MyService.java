package com.example.appwithservice;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class MyService extends IntentService {

    public MyService(){
        super("MyService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.i("Service", "Service Thread: " + Thread.currentThread().getName());
        int i = 0;
        while (true){
            Log.i("Service", "Log from service: " + i);
            Intent intent2 = new Intent();
            intent2.setAction("MY_NOTIFICATION");
            intent2.putExtra("data","broadcast NUM: " + i);
            sendBroadcast(intent2);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
    }
}
