package com.example.appwithservice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Intent i;
    static public MainActivity self;
    TextView my_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        self = this;
        my_text = findViewById(R.id.text);
        Button start_service = (Button) findViewById(R.id.start_service);
        i = new Intent(this, MyService.class);

        Log.i("onCreate", "onCreate Thread: " + Thread.currentThread().getName());
        start_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("onClick", "Log from onClick");
                startService(i);
                v.setEnabled(false);
            }
        });

        BroadcastReceiver mReceiver = new BroadcastReceiver(){
            public void onReceive(Context context, Intent intent){
                my_text.setText(intent.getStringExtra("data"));
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("MY_NOTIFICATION");
        this.registerReceiver(mReceiver, filter);

    }
}
